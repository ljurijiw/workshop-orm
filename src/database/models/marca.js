const { Sequelize, DataTypes } = require('sequelize');


function createMarcaModel(connection) {
    const Marca = connection.define('Marca', {
        id:{
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey:true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        }
    });
    return Marca;
};

module.exports = {
    createMarcaModel
}
