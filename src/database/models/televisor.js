const { Sequelize, DataTypes } = require('sequelize');


function createTelevisorModel(connection) {
    const Televisor = connection.define('Televisor', {
        id:{
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey:true
        },
        pantalla: {
            type: DataTypes.STRING,
            allowNull: false
        },
        smart: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        precio: {
            type: DataTypes.DOUBLE,
            allowNull: false
        }
    });
    return Televisor;
};

module.exports = {
    createTelevisorModel
}
