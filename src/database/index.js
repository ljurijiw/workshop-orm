const Sequelize = require("sequelize");
const { createMarcaModel } = require("./models/marca");
const { createModeloModel } = require("./models/modelo");
const { createTelevisorModel } = require("./models/televisor");
/*const { createAlbumModel } = require("./models/album");
const { createBandModel } = require('./models/band');
const { createSongModel } = require("./models/song");*/

const models = {};

async function connect(host, username, password, database) {
  const connection = new Sequelize({
    database,
    username,
    password,
    host,
    dialect: 'mysql',
    // logging: false
  });
  createTelevisorModel(connection);
  createMarcaModel(connection);
  createModeloModel(connection);

  models.Televisor = createTelevisorModel(connection);
  models.Modelo = createModeloModel(connection);
  models.Marca = createMarcaModel(connection);

  models.Marca.hasMany(models.Modelo);
  models.Modelo.belongsTo(models.Marca);

  models.Marca.hasOne(models.Televisor);
  models.Modelo.hasOne(models.Televisor);

  try {
    await connection.authenticate();
    await connection.sync();
    console.log('Connection has been established successfully.');
   
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

function getModel(name) {
  return models[name];
}

module.exports = {
  connect,
  getModel
};
