const express = require('express');
const { connect } = require('./src/database/index');
const { createTelevisorRouter } = require('./src/routers/televisor');
const server = express();
require('dotenv').config();
const PORT = process.env.PORT || 9090;

async function main() {
    const {
        DB_HOST,
        DB_USERNAME,
        DB_PASSWORD,
        DB_DATABASE
    } = process.env;

    server.listen(process.env.PORT, () => console.log(`Server is running on PORT ${PORT}`));
    server.use(express.json());
    server.use(express.urlencoded({extended: false}));
    //Agregar rutas
    server.use(createTelevisorRouter());
    await connect(DB_HOST, DB_USERNAME, DB_PASSWORD , DB_DATABASE);
}
    
main();
