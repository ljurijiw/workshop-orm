const { Router } = require('express');
const { getModel } = require('../database');

function createTelevisorRouter(params) {
    const router = new Router();

    router.get('/televisores/', async (req, resp) => {
        try {
            const data = await getModel('Televisor').findAll();
            resp.status(200).send(data);
        } catch (error) {
            resp.status(500).send({message: error.message});
        }
    });

    router.post('/televisores/', async (req, resp) => {
        const Televisor = getModel('Televisor');
        const data = new Televisor(req.body);
        const saved = await data.save()
        resp.status(201).send(saved);
    });

    return router;
}

module.exports = {
    createTelevisorRouter
}