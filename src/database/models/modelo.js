const { Sequelize, DataTypes } = require('sequelize');


function createModeloModel(connection) {
    const Modelo = connection.define('Modelo', {
        id : {
            type: DataTypes.INTEGER,
            allowNull:false,
            primaryKey:true
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
        }
    });
    return Modelo;
};

module.exports = {
    createModeloModel
}
